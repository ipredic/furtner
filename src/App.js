import React, { Component } from "react";
import axios from "axios";

import ResponsiveContainer from "./components/Layout/ResponsiveContainer";
import CarsSection from "./components/Layout/CarsSection";
import { Item, Label, Divider, Dimmer, Loader } from "semantic-ui-react";
import Footer from "./components/Layout/Footer";
import CarModal from "./components/Modal/Modal";

export default class App extends Component {
  state = {
    cars: [],
    isModalOpen: false,
    carName: null,
    carImages: [],
    carKm: null,
    carPrice: null,
    carEz: null,
    carPs: null,
    carTreibstoff: null,
    carFarbe: null,
    carKategorie: null,
    carAufbau: null,
    carGetriebe: null,
    carAntrieb: null,
    carTuren: null,
    carSitze: null,
    carBeschreibung: null
  };

  componentDidMount = () => {
    this.handleFetchCars();
  };

  handleFetchCars = async () => {
    try {
      const response = await axios.get(
        "https://ivanp14.pythonanywhere.com/carlist",
        {
          headers: {
            contentType: "application/json",
            "Access-Control-Allow-Origin": "*"
          }
        }
      );
      this.setState({
        cars: response.data.cars
      });
    } catch (error) {
      console.log(error);
      //ignore errors
    }
  };

  handleShowDataInModal = id => {
    this.state.cars.filter(el =>
      el.car_id === id
        ? this.setState({
            isModalOpen: true,
            carName: el.car_name,
            carImages: el.car_image,
            carKm: el.car_km,
            carPrice: el.car_price,
            carEz: el.car_ez,
            carPs: el.car_ps,
            carTreibstoff: el.car_treibstoff,
            carFarbe: el.car_farbe,
            carKategorie: el.car_kategorie,
            carAufbau: el.car_aufbau,
            carGetriebe: el.car_getriebe,
            carAntrieb: el.car_antrieb,
            carTuren: el.car_turen,
            carSitze: el.car_sitze,
            carBeschreibung: el.car_beschreibung
          })
        : null
    );
  };

  handleCloseModal = () => {
    this.setState({ isModalOpen: !this.state.isModalOpen });
  };

  render() {
    if (this.state.cars.length !== 0) {
      return (
        <ResponsiveContainer>
          <CarModal
            modalOpen={this.state.isModalOpen}
            onModalClose={() => this.handleCloseModal()}
            carImages={this.state.carImages}
            carName={this.state.carName}
            carKm={this.state.carKm}
            carPrice={this.state.carPrice}
            carEz={this.state.carEz}
            carPs={this.state.carPs}
            carTreibstoff={this.state.carTreibstoff}
            carFarbe={this.state.carFarbe}
            carKategorie={this.state.carKategorie}
            carAufbau={this.state.carAufbau}
            carGetriebe={this.state.carGetriebe}
            carAntrieb={this.state.carAntrieb}
            carTuren={this.state.carTuren}
            carSitze={this.state.carSitze}
            carBeschreibung={this.state.carBeschreibung}
          />
          <CarsSection>
            {this.state.cars.map(car => (
              <Item.Group divided key={car.car_id}>
                <Item
                  style={{ cursor: "pointer" }}
                  onClick={() => this.handleShowDataInModal(car.car_id)}
                >
                  <Item.Image size="small" src={car.car_image[0]} />
                  <Item.Content>
                    <Item.Header>{car.car_name}</Item.Header>
                    <Item.Meta>
                      <span className="price">Auto Daten</span>
                    </Item.Meta>
                    <Item.Description>
                      {car.car_kategorie}, {car.car_treibstoff},{" "}
                      {car.car_aufbau}
                    </Item.Description>
                    <Item.Extra>
                      <Label.Group tag>
                        <Label color="black">EZ: {car.car_ez}</Label>
                        <Label color="black">PS: {car.car_ps}</Label>
                        <Label color="black">KM: {car.car_km}</Label>
                        <Label color="black">{car.car_price} €</Label>
                      </Label.Group>
                    </Item.Extra>
                  </Item.Content>
                </Item>
                <Divider />
              </Item.Group>
            ))}
          </CarsSection>
          <Footer />
        </ResponsiveContainer>
      );
    }
    return (
      <Dimmer active inverted>
        <Loader size="large">Loading</Loader>
      </Dimmer>
    );
  }
}
