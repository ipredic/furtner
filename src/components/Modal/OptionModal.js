import React from "react";
import { Button, Modal, Icon, Image } from "semantic-ui-react";
import "./../../global.css";

const OptionModal = ({ children, modalOpen, onModalClose, header, image }) => (
  <Modal open={modalOpen}>
    <Modal.Header>{header}</Modal.Header>
    <Modal.Content image>
      <Image wrapped size="massive" src={image} />
      <Modal.Description>
        <Modal.Content>{children}</Modal.Content>
      </Modal.Description>
    </Modal.Content>
    <Modal.Actions>
      <Button color="black" onClick={onModalClose}>
        <Icon name="remove" /> Schließen
      </Button>
    </Modal.Actions>
  </Modal>
);

export default OptionModal;
