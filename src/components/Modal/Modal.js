import React from "react";
import { Button, Modal, Label, Icon, Segment } from "semantic-ui-react";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import "./../../global.css";
import { Carousel } from "react-responsive-carousel";

const CarModal = props => (
  <Modal open={props.modalOpen}>
    <Modal.Header>{props.carName}</Modal.Header>
    <Carousel showIndicators={false} autoPlay>
      {props.carImages.map(i => (
        <img src={i} key={i} alt="img" />
      ))}
    </Carousel>
    <Modal.Content>
      <Label color="black" basic>
        KM: {props.carKm}
      </Label>
      <Label color="black" basic>
        PREIS: {props.carPrice}€
      </Label>
      <Label color="black" basic>
        EZ: {props.carEz}
      </Label>
      <Label color="black" basic>
        PS: {props.carPs}
      </Label>
      <Label color="black" basic>
        TREIBSTOFF: {props.carTreibstoff}
      </Label>
      <Label color="black" basic>
        FARBE: {props.carFarbe}
      </Label>
      <Label color="black" basic>
        KATEGORIE: {props.carKategorie}
      </Label>
      <Label color="black" basic>
        AUFBAU: {props.carAufbau}
      </Label>
      <Label color="black" basic>
        GETRIEBE: {props.carGetriebe}
      </Label>
      <Label color="black" basic>
        ANTRIEB: {props.carAntrieb}
      </Label>
      <Label color="black" basic>
        TÜREN: {props.carTuren}
      </Label>
      <Label color="black" basic>
        SITZE: {props.carSitze}
      </Label>
      <Segment color="black" style={{ wordBreak: "break-all" }}>
        <h4>{props.carBeschreibung}</h4>
      </Segment>
    </Modal.Content>
    <Modal.Actions>
      <Button color="black" onClick={props.onModalClose}>
        <Icon name="remove" /> Schließen
      </Button>
    </Modal.Actions>
  </Modal>
);

export default CarModal;
