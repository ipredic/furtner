import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Container,
  Icon,
  Menu,
  Responsive,
  Segment,
  Sidebar,
  List,
  Image
} from "semantic-ui-react";
import getWidth from "./getWidth";
import HomepageHeading from "./HomepageHeading";
import finanzJPG from "../../assets/finanz.jpg";
import BankLogo from "../../assets/bank.jpg";
import VerkaufenJPG from "../../assets/verkaufen.jpg";
import OptionModal from "../Modal/OptionModal";
import "../../global.css";

export default class MobileContainer extends Component {
  state = {
    isVerkaufenModal: false,
    isFinanzirungModalOpen: false
  };

  handleSidebarHide = () => this.setState({ sidebarOpened: false });

  handleToggle = () => this.setState({ sidebarOpened: true });

  render() {
    const { children } = this.props;
    const { sidebarOpened } = this.state;

    return (
      <Responsive
        as={Sidebar.Pushable}
        getWidth={getWidth}
        maxWidth={Responsive.onlyMobile.maxWidth}
      >
        <OptionModal
          modalOpen={this.state.isVerkaufenModal}
          header="FAHRZEUGANKAUF"
          onModalClose={() => this.setState({ isVerkaufenModal: false })}
          image={VerkaufenJPG}
        >
          <strong>WIR KAUFEN ALLE ARTEN VON FAHRZEUGEN AN!</strong>
          <br />
          Wenn Sie in Wien einen Gebrauchtwagen verkaufen wollen gibt es viele
          Möglichkeiten, aber nur beim Furtner's Mobile Wien bekommen Sie
          garantiert die besten Preise. Sie müssen uns nur anrufen, schon kommen
          wir vorbei und schauen uns Ihr Fahrzeug an. Es ist egal welche Marke
          oder welches Modell es ist, der Furtner's Mobile Wien kauft jedes
          Auto.
        </OptionModal>
        <OptionModal
          modalOpen={this.state.isFinanzirungModalOpen}
          onModalClose={() => this.setState({ isFinanzirungModalOpen: false })}
          header="FINANZIEREN"
          image={finanzJPG}
        >
          <strong>
            Wir prüfen mit Ihnen gemeinsam die für Sie passende
            Finanzierungslösung:{" "}
          </strong>
          <List.Item>
            50/50 Finanzierung Drittelfinanzierung klassischer Konsumkredit Ob
            klassisches Ratenmodell mit-/ohne Anzahlung-/Restkredit oder
            Drittelfinanzierung bzw.
          </List.Item>
          <List.Item>
            50/50 Finanzierung (Hälfte sofort/Hälfte in 1 Jahr) etc. Sie sehen
            schon, es gibt zahlreiche Möglichkeiten die wir bei einem
            persönlichen Gespräch gerne besprechen können. Bei{" "}
            <strong>Furtner's</strong> autohaus in Wien finden wir zusammen mit
            Ihnen immer die Richtige Lösung, ob es die Wahl des Fahrzeuges ist
            oder die richtige Finanzierung.
          </List.Item>
          <br />
          <strong>Unser Finanzierungspartner:</strong>
          <Image size="medium" src={BankLogo} />
        </OptionModal>
        <Sidebar
          as={Menu}
          animation="push"
          inverted
          onHide={this.handleSidebarHide}
          vertical
          visible={sidebarOpened}
        >
          <Menu.Item onClick={this.handleSidebarHide} as="a" href="#carlist">
            Fahrzeugangebot
          </Menu.Item>
          <Menu.Item onClick={this.handleSidebarHide} as="a" href="#kontakt">
            Kontakt
          </Menu.Item>
          <Menu.Item
            onClick={() =>
              this.setState({
                isFinanzirungModalOpen: true,
                sidebarOpened: false
              })
            }
          >
            Finanzierung
          </Menu.Item>
          <Menu.Item
            onClick={() =>
              this.setState({
                isVerkaufenModal: true,
                sidebarOpened: false
              })
            }
          >
            Verkuafen
          </Menu.Item>
        </Sidebar>

        <Sidebar.Pusher dimmed={sidebarOpened}>
          <Segment
            inverted
            textAlign="center"
            style={{ minHeight: 350, padding: "1em 0em" }}
            vertical
            id="mobilebg"
          >
            <Container>
              <Menu inverted pointing secondary size="large">
                <Menu.Item onClick={this.handleToggle}>
                  <Icon name="sidebar" />
                </Menu.Item>
              </Menu>
            </Container>
            <HomepageHeading mobile />
          </Segment>
          {children}
        </Sidebar.Pusher>
      </Responsive>
    );
  }
}

MobileContainer.propTypes = {
  children: PropTypes.node
};
