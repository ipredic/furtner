import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Container,
  Menu,
  Responsive,
  Segment,
  Visibility,
  List,
  Image
} from "semantic-ui-react";
import HomepageHeading from "./HomepageHeading";
import OptionModal from "../Modal/OptionModal";
import BankLogo from "../../assets/bank.jpg";
import getWidth from "./getWidth";
import finanzJPG from "../../assets/finanz.jpg";
import VerkaufenJPG from "../../assets/verkaufen.jpg";
import "../../global.css";

export default class DesktopContainer extends Component {
  state = {
    isFinanzirungModalOpen: false,
    isVerkaufenModal: false
  };

  hideFixedMenu = () => this.setState({ fixed: false });
  showFixedMenu = () => this.setState({ fixed: true });

  render() {
    const { children } = this.props;

    return (
      <Responsive getWidth={getWidth} minWidth={Responsive.onlyTablet.minWidth}>
        <Visibility
          once={false}
          onBottomPassed={this.showFixedMenu}
          onBottomPassedReverse={this.hideFixedMenu}
        >
          <OptionModal
            modalOpen={this.state.isVerkaufenModal}
            header="FAHRZEUGANKAUF"
            onModalClose={() => this.setState({ isVerkaufenModal: false })}
            image={VerkaufenJPG}
          >
            <strong>WIR KAUFEN ALLE ARTEN VON FAHRZEUGEN AN!</strong>
            <br />
            Wenn Sie in Wien einen Gebrauchtwagen verkaufen wollen gibt es viele
            Möglichkeiten, aber nur beim Furtner's Mobile Wien bekommen Sie
            garantiert die besten Preise. Sie müssen uns nur anrufen, schon
            kommen wir vorbei und schauen uns Ihr Fahrzeug an. Es ist egal
            welche Marke oder welches Modell es ist, der Furtner's Mobile Wien
            kauft jedes Auto.
          </OptionModal>
          <OptionModal
            modalOpen={this.state.isFinanzirungModalOpen}
            onModalClose={() =>
              this.setState({ isFinanzirungModalOpen: false })
            }
            header="FINANZIEREN"
            image={finanzJPG}
          >
            <strong>
              Wir prüfen mit Ihnen gemeinsam die für Sie passende
              Finanzierungslösung:{" "}
            </strong>
            <List.Item>
              50/50 Finanzierung Drittelfinanzierung klassischer Konsumkredit Ob
              klassisches Ratenmodell mit-/ohne Anzahlung-/Restkredit oder
              Drittelfinanzierung bzw.
            </List.Item>
            <List.Item>
              50/50 Finanzierung (Hälfte sofort/Hälfte in 1 Jahr) etc. Sie sehen
              schon, es gibt zahlreiche Möglichkeiten die wir bei einem
              persönlichen Gespräch gerne besprechen können. Bei{" "}
              <strong>Furtner's</strong> autohaus in Wien finden wir zusammen
              mit Ihnen immer die Richtige Lösung, ob es die Wahl des Fahrzeuges
              ist oder die richtige Finanzierung.
            </List.Item>
            <br />
            <strong>Unser Finanzierungspartner:</strong>
            <Image size="medium" src={BankLogo} />
          </OptionModal>
          <Segment inverted textAlign="center" vertical id="container__header">
            <Menu secondary borderless>
              <Container id="nav">
                <Menu.Item style={{ color: "#fff" }} as="a" href="#carslist">
                  Fahrzeugangebot
                </Menu.Item>
                <Menu.Item style={{ color: "#fff" }} href="#kontakt" as="a">
                  Kontakt
                </Menu.Item>
                <Menu.Item
                  style={{ color: "#fff" }}
                  onClick={() =>
                    this.setState({ isFinanzirungModalOpen: true })
                  }
                >
                  Finanzierung
                </Menu.Item>
                <Menu.Item
                  style={{ color: "#fff" }}
                  onClick={() => this.setState({ isVerkaufenModal: true })}
                >
                  Verkuafen
                </Menu.Item>
              </Container>
            </Menu>
            <HomepageHeading />
          </Segment>
        </Visibility>
        {children}
      </Responsive>
    );
  }
}

DesktopContainer.propTypes = {
  children: PropTypes.node
};
