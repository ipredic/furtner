import React from "react";
import PropTypes from "prop-types";
import { Container, Header, Image } from "semantic-ui-react";
import Logo from "../../assets/furtnerlogo.png";
import "../../global.css";

const HomepageHeading = ({ mobile }) => (
  <Container>
    <Header
      as="h1"
      content={<Image id="logo" src={Logo} />}
      inverted
      style={{
        fontSize: mobile ? "2em" : "4em",
        fontWeight: "normal",
        marginBottom: 0,
        marginTop: mobile ? "1.5em" : "3em"
      }}
    />
  </Container>
);

HomepageHeading.propTypes = {
  mobile: PropTypes.bool
};

export default HomepageHeading;
