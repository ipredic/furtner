import React from "react";
import {
  Container,
  Grid,
  Header,
  Segment,
  Icon,
  Image
} from "semantic-ui-react";
import maps from "../../assets/maps.png";
import logo from "../../assets/furtnerlogo.png";
import "../../global.css";

const Footer = () => (
  <Segment inverted vertical style={{ padding: "5em 0em", background: "#000" }}>
    <Container id="kontakt">
      <Grid divided inverted stackable id="footer__content">
        <Grid.Row>
          <Grid.Column width={2}>
            <Image src={logo} />
          </Grid.Column>
          <Grid.Column width={7}>
            <Header as="h4" inverted>
              Furtner´s Mobile Adresse
            </Header>
            <p>Gumpendorfer Straße 65 1060 Wien, 06. Bezirk, Mariahilf</p>
            <Image src={maps} />
          </Grid.Column>
          <Grid.Column width={3}>
            <Header inverted as="h4" content="Kontakt" />
            Termine nach telefonischer vereinbarung
            <br />
            <br />
            <Icon name="call" />
            <a style={{ color: "#fff" }} href="tel:+43 699 11111092">
              <span>+43 699 11111092</span>
            </a>
            <br />
            <br />
            <Icon name="call" />
            <a style={{ color: "#fff" }} href="tel:+43 6609319369">
              <span>+43 660 9319369</span>
            </a>
            <br />
            <br />
            <Icon name="mail" />
            <a style={{ color: "#fff" }} href="mailto:infofurtner@gmail.com">
              <span>infofurtner@gmail.com</span>
            </a>
            <br />
            <br />
            <Icon name="mail" />
            <a style={{ color: "#fff" }} href="mailto:info@zumfurtner.at">
              <span>info@zumfurtner.at</span>
            </a>
          </Grid.Column>
        </Grid.Row>
        <p>
          <span>&#9400;</span>Furtner´s Mobile 2019
        </p>
      </Grid>
    </Container>
  </Segment>
);

export default Footer;
