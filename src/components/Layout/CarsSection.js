import React from "react";
import { Grid, Header, Segment } from "semantic-ui-react";
import "../../global.css";

const CarsSection = ({ children }) => (
  <Segment style={{ padding: "8em 0em" }} vertical id="carslist">
    <Grid container stackable verticalAlign="middle" id="cars__section">
      <Header
        as="h1"
        style={{
          fontSize: "3em",
          borderBottom: "2px solid #000",
          width: "92%",
          textAlign: "center"
        }}
      >
        Aktuelle Fahrzeuge
      </Header>
      <Grid.Row>
        <Grid.Column
          width={12}
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          {children}
        </Grid.Column>
      </Grid.Row>
    </Grid>
  </Segment>
);

export default CarsSection;
